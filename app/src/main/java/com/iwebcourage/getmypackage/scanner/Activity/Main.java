package com.iwebcourage.getmypackage.scanner.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.BuildConfig;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.iwebcourage.getmypackage.scanner.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Main extends AppCompatActivity {

    private IntentIntegrator integrator = null;
    public static final String CODE_VALUE = "com.iwebcourage.getmypackage.scanner.CODE.RESULT";
    public static final int CODE_REQUEST = 5;

    public static void show(Activity activity){
        Intent intent = new Intent(activity, Main.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        TextView zxing = (TextView) findViewById(R.id.avt_main_btn_zxing);
        assert zxing != null;
        zxing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator = new IntentIntegrator(Main.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();
            }
        });

        final TextView scandid = (TextView) findViewById(R.id.avt_main_btn_scandid);
        assert scandid != null;
        scandid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSampleActivity.show(Main.this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(RESULT_OK == resultCode){
            TextView txt = (TextView) findViewById(R.id.text_scan);
            if(requestCode == CODE_REQUEST){
                String info = (String) data.getExtras().get(CODE_VALUE);
                txt.setText(info);
            }else {
                IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if (scanningResult != null) {
                    String scanContent = scanningResult.getContents();
                    String scanFormat = scanningResult.getFormatName();

                    txt.setText("FORMAT : " + scanFormat + "\nCONTENT : " + scanContent);
                    String contents = data.getStringExtra("SCAN_RESULT");
                    String format = data.getStringExtra("SCAN_RESULT_FORMAT");
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "No scan data received!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }


    }
}
