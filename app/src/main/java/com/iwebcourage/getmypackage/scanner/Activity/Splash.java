package com.iwebcourage.getmypackage.scanner.Activity;

import android.app.Activity;
import android.os.Bundle;

import com.iwebcourage.getmypackage.scanner.R;

import java.util.Timer;
import java.util.TimerTask;

public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_splash);

        new Timer("Splash Timer").schedule(new TimerTask() {
            @Override
            public void run() {
                Main.show(Splash.this);
                Splash.this.finish();
            }
        }, 3000);
    }
}
